use embed_manifest::manifest::HeapType;
use embed_manifest::{embed_manifest, new_manifest};

fn main() {
    embed_manifest(new_manifest("Carey.Background").heap_type(HeapType::SegmentHeap))
        .expect("unable to embed manifest file");
    println!("cargo:rerun-if-changed=build.rs");
}
