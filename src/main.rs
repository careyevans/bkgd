#![windows_subsystem = "windows"]

use std::process::exit;

use windows::core::w;
use windows::Win32::Foundation::{COLORREF, HWND};
use windows::Win32::Graphics::Gdi::{SetSysColors, COLOR_BACKGROUND};
use windows::Win32::UI::WindowsAndMessaging::{MessageBoxW, MB_ICONERROR, MB_OK};

fn main() {
    let elements = [COLOR_BACKGROUND.0];
    let rgb_values = [COLORREF(0x00a56e33)];
    if let Err(err) = unsafe { SetSysColors(1, elements.as_ptr(), rgb_values.as_ptr()) } {
        unsafe {
            MessageBoxW(
                HWND::default(),
                &err.message(),
                w!("SetSysColors failed"),
                MB_OK | MB_ICONERROR,
            );
        }
        exit(1)
    }
}
